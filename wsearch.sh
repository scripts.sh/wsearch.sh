#!/usr/bin/env bash

#  wsearch.sh: Menu interface for web searching.
#  Copyright (C) 2023  abgdhoz
#  
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ABOUT: This script searches the web using ddgr (command line web searching tool throgh DuckDuckGo
#       search engine) and prompt the user to enter the search keyword using rofi (run launcher and
#       more). The search results will be displayed in rofi menu.  The chosen page from the menu
#       will be visited in the browser defined in 'BROWSER' variable.

#DEPENDENCY: rofi, ddgr.

#USAGE: ./wsearch.sh [OPTIONS]...

#-b [BROWSER]:-------------Web browser to open the chosen page. It could be any web browser ether a
#                          GUI or text-based browser.  It defaults to the user default browser (that
#                          run with 'xdg-open URL'), If browsers is text-based that runs inside a
#                          terminal '-t' option must be provided.

#-t [=TERMINAL]:-----------Terminal emulator to run BROWSER in '-t=TERMINAL'. It make sense for
#                          text-based browsers which run within a terminal such as lynx, w3m,
#                          etc. If no argument provided '-t', the terminal defined in
#                          x-terminal-emulator will be used.

#-w [WEBSITE or list]:-----The website to narrow search results from it only, it can be any website
#                          name (eg, wiki.archlinux.org).  It defaults to none (get results from any
#                          website). If you want to chose from a list use '-w list' which will lists
#                          websites in 'websites.list' file to chose from. 'websites.list' must be
#                          in the same directory as 'wsearch.sh', and each line should contains a
#                          website or 'none'.

#-n [RESULTS_NUMBER]:------Number of search results to fetch. This should be an integer between 1
#                          and 25 (1 <= N <= 25).  For more Information refer to ddgr man page.

#-h|--help > Shows this :)

HELP()
{
echo -e '\n' \
" USAGE: ./wsearch.sh [OPTIONS]..."'\n''\n' \
"  -b [BROWSER]:-------------Web browser to open the chosen page. It could be any web browser ether a"'\n' \
"                            GUI or text-based browser.  It defaults to the user default browser (that"'\n' \
"                            run with 'xdg-open URL'), If browsers is text-based that runs inside a"'\n' \
"                            terminal '-t' option must be provided."'\n''\n' \
"  -t [=TERMINAL]:-----------Terminal emulator to run BROWSER in '-t=TERMINAL'. It make sense for"'\n' \
"                            text-based browsers which run within a terminal such as lynx, w3m,"'\n' \
"                            etc. If no argument provided '-t', the terminal defined in"'\n' \
"                            x-terminal-emulator will be used."'\n''\n' \
"  -w [WEBSITE or list]:-----The website to narrow search results from it only, it can be any website"'\n' \
"                            name (eg, wiki.archlinux.org).  It defaults to none (get results from any"'\n' \
"                            website). If you want to chose from a list use '-w list' which will lists"'\n' \
"                            websites in 'websites.list' file to chose from. 'websites.list' must be"'\n' \
"                            in the same directory as 'wsearch.sh', and each line should contains a"'\n' \
"                            website or 'none'."'\n''\n' \
"  -n [RESULTS_NUMBER]:------Number of search results to fetch. This should be an integer between 1"'\n' \
"                            and 25 (1 <= N <= 25).  For more Information refer to ddgr man page."'\n''\n' \
"  -h|--help > Shows this :)"'\n''\n' \
"  wsearch.sh Copyright (C) 2023  abgdhoz"'\n' \
"  This program comes with ABSOLUTELY NO WARRANTY. This is free software,"'\n' \
"  and you are welcome to redistribute it under certain conditions."'\n''\n'
}

# Set default values. Will be used if not specefied by user.
BROWSER="xdg-open"
WEBSITE=""
SEARCH_RESULTS_NUMBER=5
USE_TERMINAL=0

# Using 'getopt' to manage command line options.
# If failed terminate the script and throu an error.
if ! TEMP=$(getopt -o 'hb:w:n:t::' --long 'help' -n 'wsearch.sh' -- "$@") ; then

    echo 'Program terminated.' >&2
    exit 1

fi

eval set -- "$TEMP"
unset TEMP

# Get the options
while true; do
    
    case "$1" in
	
	'-h'|'--help')
            HELP
            exit 0
	    ;;
	
	'-b')
	    if ! command -v "$2" &> /dev/null ; then
		echo "-b: Provided browser < '$2' > not found." >&2
		exit 1
	    fi
	    BROWSER="$2"
	    shift 2
	    continue
	    ;;
	
	'-w')
	    if [[ "$2" == "list" ]] ; then
		IFS=$'\n' read -r -d '' -a WEBSITES_LIST < $(dirname -- "${BASH_SOURCE[0]}")/websites.list
		WEBSITE_CHOSEN=$(echo "${WEBSITES_LIST[@]}" | rofi -dmenu -i -sep ' ' -p 'wsearch / select website : ') || exit 1
		if [[ -n "${WEBSITE_CHOSEN}" ]] ; then
		    if [[ "${WEBSITE_CHOSEN}" != "none" ]] ; then
			WEBSITE="${WEBSITE_CHOSEN}"
		    fi
		else
		    echo "Program terminated." >&2
		    exit 0
		fi
	    else
		WEBSITE="$2"
	    fi
	    shift 2
	    continue
	    ;;
	
	'-n')
            if [[ "$2" -lt 1 || "$2" -gt 25 ]] ; then
		echo '-n: Number of search results must be from 1 to 25 inclusive.' >&2
		exit 1
	    fi
	    SEARCH_RESULTS_NUMBER="$2"
	    shift 2
	    continue
	    ;;
	
	'-t')
	    USE_TERMINAL=1
	    case "$2" in
		'')
		    TERMINAL="x-terminal-emulator"
		    ;;
		['=']*)
		    TERMINAL=$(echo "${2//=/}")
		    if ! command -v "${TERMINAL}" &> /dev/null ; then
			echo "-t: Provided terminal < '$2' > not found." >&2
			exit 1
		    fi
		    ;;
		*)
		    echo "-t: error reading the terminal argument use '-t=TERMINAL'" >&2
		    exit 1
		    ;;
	    esac
	    shift 2
	    continue
	    ;;
	
	'--')
            shift
            break
	    ;;
	*)
	    echo 'Internal error!' >&2
	    exit 1
	    ;;
	
    esac
    
done


#$( dirname -- "${BASH_SOURCE[0]}" )

if [[ -n "${WEBSITE}" ]] ; then
    
    PROMPT="wsearch in ${WEBSITE} : "

else

    PROMPT="wsearch : "
    
fi

#Run rofi to prompt the user for search keyword. It will use the default theme. The 'listview { enabled: false;}' option will show the prompt only.
#The default prompt is 'Search : ', you can edit it as you wish. For more Information refer to rofi man page.
SEARCH_KEYWORD=$(echo "" | rofi -theme-str 'listview { enabled: false;}' -dmenu -i -p "${PROMPT}") || exit 1

#Run ddgr to search for 'SEARCH_KEYWORD', narrow results to be from 'WEBSITE' only, and save the result in 'SEARCH_RESULTS' in JSON format.
SEARCH_RESULTS=$(ddgr "${SEARCH_KEYWORD}" "${WEBSITE}" -n "${SEARCH_RESULTS_NUMBER}" --json)

#Filter search results and store their titles in 'TITLES'.
IFS=$'\n' read -r -d '' -a TITLES < <( echo "${SEARCH_RESULTS}" | \
					   grep "\"title\":" | \
					   awk -F "\"" '{print $4}' | \
					   sed -e 's/&/\&amp;/g' -e 's/</\&lt;/g' -e 's/>/\&gt;/g' -e 's/'\''/\&#39;/g' \
					   && printf '\0' )

#Filter search results and store their abstracts in 'ABSTRACT'.
IFS=$'\n' read -r -d '' -a ABSTRACT < <( echo "${SEARCH_RESULTS}" | \
					     grep "\"abstract\":" | \
					     awk -F "\"" '{print $4}' | \
					     sed -e 's/&/\&amp;/g' -e 's/</\&lt;/g' -e 's/>/\&gt;/g' -e 's/'\''/\&#39;/g' \
					     && printf '\0' )

#Filter search results and store their urls in 'URLS'.
IFS=$'\n' read -r -d '' -a URLS < <( echo "${SEARCH_RESULTS}" | \
					 grep "\"url\":" | \
					 awk '{print $2}' | \
					 awk -F "\"" '{print $2}' | \
					 sed -e 's/&/\&amp;/g' -e 's/</\&lt;/g' -e 's/>/\&gt;/g' -e 's/'\''/\&#39;/g' \
					 && printf '\0' )

# Set each TITLE with it's ABSTRACT and URL in one element. And set each line to be 140 char as maximum length.
for i in "${!TITLES[@]}"; do

    # Don't add separator(') at the end of the last element.
    # Add Pango markup to format 'TITLES' and 'URLS'.
    if [[ "${#ARRAY[@]}" -eq "${#TITLES[@]} -1" ]]; then
	
	ARRAY[i]=$(printf "%s<span foreground=\"#98be65\" size=\"large\"><b>${TITLES[i]}</b></span> \
	                   %s\n${ABSTRACT[i]} \
			   %s\n<span foreground=\"#da8548\">${URLS[i]}</span>" | \
		           sed -e "s/.\{140\} /&\n/g")
	
    else
	
	ARRAY[i]=$(printf "%s<span foreground=\"#98be65\" size=\"large\"><b>${TITLES[i]}</b></span> \
			   %s\n${ABSTRACT[i]} \
			   %s\n<span foreground=\"#da8548\">${URLS[i]}</span>'" | \
		           sed -e "s/.\{140\} /&\n/g")
	
    fi

done

#Pipe the 'ARRAY' into rofi.
CHOICE=$(echo "${ARRAY[@]}" | rofi -markup-rows -dmenu -i -l 10 -eh 6 -sep '\x27' -p 'Results : ') || exit 1


if [[ -n "${CHOICE}" ]]; then

    # If choice defined remove the 'URL' formatting.
    CHOICE=$(echo "${CHOICE}" | sed -n '$p' | sed -e "s/<span foreground=\"#da8548\">//" -e "s/<\/span>//")

    # If -t option used, run browser from the termenal.
    if [ "${USE_TERMINAL}" -ne 0 ] ; then
	
	${TERMINAL} -e "${BROWSER}" "${CHOICE}" &
	
    else
	
	# Run 'BROWSER'.
	${BROWSER} "${CHOICE}" &
	
    fi
    
else
    
    echo "Program terminated." >&2
    exit 0
    
fi
